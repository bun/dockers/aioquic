#!/bin/bash
echo "Starting the AIOQUIC Server supporting HTTP3"
python3 examples/http3_server.py --port 4443 --certificate tests/ssl_cert.pem --private-key tests/ssl_key.pem --quic-log logs &