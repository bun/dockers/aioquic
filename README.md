# Ubuntu image to test QUIC in GNS-3

## Description

This docker image will contain the AIOQUIC software supporting QUIC and HTTP/3.

### Additional packages added
The packages added to the base image are visible in the ```Dockerfile```.

## Build locally to test 

To test if the image can be build without error, test locally on your machine with the command:
```
docker build -t aioquic .
```

## Force rebuild

If we change the content of the ```Dockerfile``` we have to add a new Tag to this repo. Then the CI/CD Pipeline will be executed.
Go to the "Repository --> Code --> Tag" menu to add the latest version number.

## Usage

- To download and use this image, just type:
    ```bash
    docker pull registry.forge.hefr.ch/bun/dockers/aioquic
    ```

- To run this image and mount a local volume to retrieve the QLOG:
  ```bash
  docker run -it -p4443:4443 -v ~/mytmp/aioquic:/aioquic/logs/ registry.forge.hefr.ch/bun/dockers/aioquic bash
  ```
  This will map the directory ```~/mytmp/aioquic``` to the directory ```/aioquic/logs``` inside the container.

- Some scripts are available to start the examples available inside this library:
  - ```http3_server.sh``` : start a HTTP3 serveur with a self-signed certificate and logging connection in QLOG format inside the ```/aioquic/logs/```directory.  You can edit/add the following options:
    - **Port**: UDP port to use, default is 4433, and here it changed to 4443. 
    - **Congestion Control Algorithm** :  the available keywords are ```reno``` or ```cubic```
    - **Verbosity** : add the ```-v``` parameter
    - **Maximum datagram size**: add the ```--max-datagram-size``` (default is 65536 Bytes)
  - To use this container as a HTTP3 client, use the ```http3_client.py``` available in the ```examples``` directory. Some of the available parameters:
    - ```--insecure```: don't check the remote certificate
    - ```--quic-log```: generate QLOG file for the connection
    - ```-l```: write the SSL key in a log to decrypt the flow with wireshark
    - ```--congestion-control-algorithm```: as for the server
    - ```--legacy-http```: use HTTP/0.9
    - ```--negotiate-v2```: start with QUIC v1 and try to negotiate v2
    - ```--help```: display the additional parameters

## More informations
See Gitlab Docs:
* https://docs.gitlab.com/ee/user/packages/container_registry/


<hr>
(c) F. Buntschu 02.10.2024