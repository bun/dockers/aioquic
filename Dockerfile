FROM ubuntu:24.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y wget net-tools iputils-ping tcpdump ethtool iperf iproute2

RUN apt-get update
RUN apt-get install -y git-core libssl-dev python3 python3-dev python3-pip vim nano
RUN apt-get install -y python3-jinja2 python3-starlette python3-aioquic netcat-traditional
RUN git clone https://github.com/aiortc/aioquic && cd /aioquic && git checkout 1.2.0

# Replace the "buggy http3_client"
COPY http3_client.py /aioquic/examples

WORKDIR /aioquic

RUN mkdir logs
COPY http3_server.sh .
RUN chmod +x http3_server.sh

EXPOSE 80 4443 8443 4443/udp 8443/udp

CMD ["bash"]